import pandas as pd
from datetime import datetime
from google.cloud import bigquery
from pandas_profiling import ProfileReport





# Construct a BigQuery client object.
client = bigquery.Client()


query = """
WITH

--Tabla de Buyers Modulo Legal Comercial 

buyers_modulo_comercial_legal AS (
  SELECT *,

  -- Creación de columna con indicación del estatus que corresponde al id_phase en el que se encuentra 
  CASE 
    WHEN id_phase = '315438380' THEN 'DESISTIDO'
    WHEN id_phase = '315364179' THEN 'DESISTIDO'
    WHEN id_phase = '315406217' THEN 'DESISTIDO'
    WHEN id_phase = '315708554' THEN 'DESISTIDO'
    WHEN id_phase = '314507099' THEN 'DESISTIDO'
    WHEN id_phase = '314837537' THEN 'CIERRE'
    ELSE 'EN PROCESO'
  END AS estatus

  FROM (
    SELECT * FROM (
      SELECT *, ROW_NUMBER() OVER (PARTITION BY nid, id_pipe, cedula ORDER BY fecha_crea_ci DESC) AS unico
      FROM (
        SELECT REPLACE(TRIM(pv.nid),' ','') nid,
        pv.id_card,
        pv.fecha_crea_card,
        pv.first_time_in_2 fecha_crea_ci,
        pv.first_time_in first_time_card_in_phase,
        pv.name_pipe,
        pv.id_pipe,
        pv.name_phase,
        pv.id_phase,
        REPLACE(REPLACE(REPLACE(TRIM(pv.numerodeidentificacioncliente1),' ',''),'.',''),',','') cedula,
        pv.tipo_de_venta,
        pv.correo_del_broker,
        -- pv.desistido,
        pv.comercial,
        pv.fases_por_card,
        -- pv.fecha_desistimiento,
        -- pv.fechadepagoarras fecha_pago_arras,
        -- pv.fechatentativadefirmapromesa fecha_tentativa_firma_pc,
        -- pv.fecha_de_pcv_seg_n_ci,
        -- pv.fecha_de_firma_pcv fecha_de_firma_pcv_real,
        -- pv.fecha_real_pago_de_arras,
        -- pv.fechadepagoseparacion fecha_pago_separacion_ci,
        -- pv.fechatentativadefirmadeseparacion fecha_tentativa_firma_ci,
        -- pv.fechatentativadefirmaescritura_1 fecha_tentativa_escritura,
        -- pv.desistido_por_arras_1 fecha_desistimiento_real_arras
        FROM (
          SELECT 
          pc.id id_card,
          pc.title nid,
          pp.id id_pipe,
          pp.name name_pipe,
          pp2.id id_phase,
          pp2.name name_phase,
          pf.minimal_field_id id_field,
          pf.value,
          pc.created_at fecha_crea_card,
          hc2.first_time_in first_time_in_2,
          hc.first_time_in,
          hcg.fases_por_card
          FROM habi_wh_pipefy.pipefy_pipe pp
          LEFT JOIN habi_wh_pipefy.pipefy_phase pp2 ON pp.id = pp2.pipe_id       
          LEFT JOIN habi_wh_pipefy.pipefy_card pc ON pp.id = pc.pipe_id
          AND pp2.id = pc.current_phase_id
          LEFT JOIN habi_wh_pipefy.pipefy_field pf ON pc.id = pf.card_id  
          LEFT JOIN habi_wh_pipefy.pipefy_card_phases_history AS hc on pc.id = hc.card_id AND pp2.id = hc.phase_id
          LEFT JOIN habi_wh_pipefy.pipefy_card_phases_history AS hc2 on pc.id = hc2.card_id AND hc2.phase_id = '314507096'
          LEFT JOIN (SELECT card_id,COUNT(*) AS fases_por_card FROM habi_wh_pipefy.pipefy_card_phases_history AS hc
                     GROUP BY card_id) hcg ON hcg.card_id = pc.id

          -- Filtro de fecha, solo queremos traer los negocios de Noviembre a la fecha
          WHERE pc.created_at >= '2022-11-01'	
          AND LENGTH(pc.title) IN (10,11)
          AND pp.id IN (
                      '302282489'  --  Buyers Modulo Comercial/Legal
                  -- ,'302340135'  --  Carta de inteción Buyers
                  -- ,'302736943'  --  Inmobiliaria
                  -- ,'302295622'  --  Escrituración Buyers
                    )
        ) BASE
        PIVOT (
          MAX(BASE.value) FOR BASE.id_field IN (
            'numerodeidentificacioncliente1',
            'tipo_de_venta',
            'correo_del_broker',
            -- 'desistido',
            'comercial'
            -- 'fecha_desistimiento',
            -- 'fechadepagoarras',
            -- 'fechatentativadefirmapromesa',
            -- 'fecha_de_pcv_seg_n_ci',
            -- 'fechadepagoarras',
            -- 'fecha_real_pago_de_arras',
            -- 'fechadepagoseparacion',
            -- 'fechatentativadefirmadeseparacion',
            -- 'fechatentativadefirmaescritura_1'
            -- 'desistido_por_arras_1'
          )
        ) pv
      )
    ) WHERE unico = 1
  )
),

--Tabla de Buyers Promesa de Compraventa

buyers_promesa_de_compraventa AS (
  SELECT *,

  -- Creación de columna con indicación del estatus que corresponde al id_phase en el que se encuentra
  CASE 
    WHEN id_phase = '315595048' THEN 'DESISTIDO'
    WHEN id_phase = '314837593' THEN 'CIERRE'
    ELSE 'EN PROCESO'
  END AS estatus

  FROM (
    SELECT * FROM (
      SELECT *, ROW_NUMBER() OVER (PARTITION BY nid, id_pipe, cedula ORDER BY fecha_crea_pc DESC) AS unico
      FROM (
        SELECT REPLACE(TRIM(pv.nid),' ','') nid,
        pv.id_card,
        pv.fecha_crea_card,
        pv.first_time_in_2 fecha_crea_pc,
        pv.first_time_in first_time_card_in_phase,
        pv.name_pipe,
        pv.id_pipe,
        pv.name_phase,
        pv.id_phase,
        REPLACE(REPLACE(REPLACE(TRIM(pv.numerodeidentificacioncliente1_1),' ',''),'.',''),',','') cedula,
        pv.tipo_de_venta,
        pv.correo_del_broker,
        --   pv.desistido,
        pv.comercial,
        pv.fases_por_card,
        --   pv.fecha_desistimiento,
        --  pv.fecha_de_pcv_seg_n_ci fecha_tentativa_firma_pc,
        --  pv.fecha_de_pcv_seg_n_ci,
        --  pv.fecha_de_firma_pcv fecha_de_firma_pcv_real,
        --  pv.fecha_de_pago_de_arras fecha_pago_arras,
        --  pv.fecha_de_pago_separaci_n fecha_pago_separacion_ci,
        --  pv.fechatentativadefirmadeseparacion fecha_tentativa_firma_ci,
        --  pv.fecha_tentativa_de_escritura fecha_tentativa_firma_escritura,
        --  pv.desistido_por_arras_1 fecha_desistimiento_real_arras
        FROM (
          SELECT pc.id id_card,
          pc.title nid,
          pp.id id_pipe,
          pp.name name_pipe,
          pp2.id id_phase,
          pp2.name name_phase,
          pf.minimal_field_id id_field,
          pf.value,
          pc.created_at fecha_crea_card,
          hc2.first_time_in first_time_in_2,
          hc.first_time_in,
          hcg.fases_por_card
          FROM habi_wh_pipefy.pipefy_pipe pp
          LEFT JOIN habi_wh_pipefy.pipefy_phase pp2 ON pp.id = pp2.pipe_id       
          LEFT JOIN habi_wh_pipefy.pipefy_card pc ON pp.id = pc.pipe_id
          AND pp2.id = pc.current_phase_id
          LEFT JOIN habi_wh_pipefy.pipefy_field pf ON pc.id = pf.card_id  
          LEFT JOIN habi_wh_pipefy.pipefy_card_phases_history AS hc on pc.id = hc.card_id AND pp2.id = hc.phase_id
          LEFT JOIN habi_wh_pipefy.pipefy_card_phases_history AS hc2 on pc.id = hc2.card_id AND hc2.phase_id = '314837591'
          LEFT JOIN (SELECT card_id,COUNT(*) AS fases_por_card FROM habi_wh_pipefy.pipefy_card_phases_history AS hc
                    GROUP BY card_id) hcg ON hcg.card_id = pc.id

          -- Filtro de fecha, solo queremos traer los negocios de Noviembre a la fecha
          WHERE pc.created_at >= '2022-11-01'	
          AND LENGTH(pc.title) IN (10,11)
          AND pp.id IN (
                    --  '302282489'    -- Buyers Modulo Comercial/Legal
                        '302340135'    -- Carta de inteción Buyers
                    -- ,'302736943'    -- Inmobiliaria
                    -- ,'302295622'    -- Escrituración Buyers
                    )
        ) BASE
        PIVOT (
          MAX(BASE.value) FOR BASE.id_field IN (
            'numerodeidentificacioncliente1_1',
            'tipo_de_venta',
            'correo_del_broker',
            'comercial'
            --  'fecha_de_pcv_seg_n_ci',
            --  'fecha_de_firma_pcv',
            --  'fecha_de_pago_de_arras',
            -- 'fecha_de_pago_separaci_n',
            --  'fechatentativadefirmadeseparacion',
            --  'fecha_tentativa_de_escritura'
            )
          ) pv
        )
      ) WHERE unico = 1
    )
  ),

--Tabla de Buyers Escrituración

buyers_escrituracion AS (
  SELECT *,

  -- Creación de columna con indicación del estatus que corresponde al id_phase en el que se encuentra
  CASE 
    WHEN id_phase = '315464011' THEN 'DESISTIDO'
    WHEN id_phase = '314583276' THEN 'CIERRE'
    ELSE 'EN PROCESO'
  END AS estatus
  
  FROM (
    SELECT * FROM (
      SELECT *, ROW_NUMBER() OVER (PARTITION BY nid, id_pipe, comercial ORDER BY fecha_crea_escrituracion DESC) AS unico  
      FROM (
        SELECT REPLACE(TRIM(pv.nid),' ','') nid,
        pv.id_card,
        pv.fecha_crea_card,
        pv.first_time_in_2 fecha_crea_escrituracion,
        pv.first_time_in first_time_card_in_phase,
        pv.name_pipe,
        pv.id_pipe,
        pv.name_phase,
        pv.id_phase,
        REPLACE(REPLACE(REPLACE(TRIM(pv.n_mero_de_identificaci_n_cliente_1),' ',''),'.',''),',','') cedula,
        pv.tipo_de_venta,
        pv.correo_del_broker,
        -- pv.desistido,
        pv.comercial,
        pv.fases_por_card,
        -- pv.fecha_desistimiento,
        -- pv.fecha_tentativa_de_firma_promesa fecha_tentativa_firma_pc,
        -- pv.fecha_de_pcv_seg_n_ci,
        -- pv.fecha_de_firma_pcv fecha_de_firma_pcv_real,
        -- pv.fecha_pago_de_arras fecha_pago_arras,
        -- pv.fecha_de_pago_separaci_n fecha_pago_separacion_ci,
        -- pv.fecha_tentativa_de_firma_de_separaci_n fecha_tentativa_firma_ci,
        -- pv.fecha_tentativa_de_escritura fecha_tentativa_firma_escritura,
        -- pv.fecha_firma_cliente fecha_firma_escritura,
        FROM (
          SELECT pc.id id_card,
            pc.title nid,
            pp.id id_pipe,
            pp.name name_pipe,
            pp2.id id_phase,
            pp2.name name_phase,
            pf.minimal_field_id id_field,
            pf.value,
            pc.created_at fecha_crea_card,
            hc2.first_time_in first_time_in_2,
            hc.first_time_in,
            hcg.fases_por_card
            FROM habi_wh_pipefy.pipefy_pipe pp
            LEFT JOIN habi_wh_pipefy.pipefy_phase pp2 ON pp.id = pp2.pipe_id       
            LEFT JOIN habi_wh_pipefy.pipefy_card pc ON pp.id = pc.pipe_id
            AND pp2.id = pc.current_phase_id
            LEFT JOIN habi_wh_pipefy.pipefy_field pf ON pc.id = pf.card_id  
            LEFT JOIN habi_wh_pipefy.pipefy_card_phases_history AS hc on pc.id = hc.card_id AND pp2.id = hc.phase_id
            LEFT JOIN habi_wh_pipefy.pipefy_card_phases_history AS hc2 on pc.id = hc2.card_id AND hc2.phase_id = '314583271'
            LEFT JOIN (SELECT card_id,count(*) as fases_por_card FROM habi_wh_pipefy.pipefy_card_phases_history AS hc
                                GROUP BY card_id) hcg on hcg.card_id = pc.id

            -- Filtro de fecha, solo queremos traer los negocios de Noviembre a la fecha
            WHERE pc.created_at >= '2022-11-01'	
            AND LENGTH(pc.title) IN (10,11)
            AND pp.id IN (
                      --  '302282489'  -- Buyers Modulo Comercial/Legal
                      --  '302340135'  -- Carta de inteción Buyers
                      -- ,'302736943'  -- Inmobiliaria
                          '302295622'  -- Escrituración Buyers
                        )
        ) BASE
        PIVOT (
          MAX(BASE.value) FOR BASE.id_field IN (
          'n_mero_de_identificaci_n_cliente_1',
          'tipo_de_venta',
          'correo_del_broker',
          'comercial'
          -- 'fecha_tentativa_de_firma_promesa',
          --  'fecha_de_firma_pcv',
          -- 'fecha_pago_de_arras',
          -- 'fecha_de_pago_separaci_n',
          -- 'fecha_tentativa_de_firma_de_separaci_n',
          -- 'fecha_tentativa_de_escritura','fecha_firma_cliente'
        
          )
        )pv
      )
    )WHERE unico = 1
  )
),

-- Creación de la tabla buyers_master que contiene las columnas de mayor interes para el análisis
buyers_master AS(

  -- Selección de las columnas nid de las tablas de Comercial Legal, Promesa de Compraventa y Escrituración para no perder los datos después de hacer el Outer
  SELECT COALESCE(cl.nid,pc.nid,es.nid) AS nid, 

  -- Llamado de  las columnas de cedulas
  cl.cedula AS cedula_comercial_legal, 
  pc.cedula AS cedula_promesa_de_compraventa,
  es.cedula AS cedula_escrituracion,

  --Llamado de las columnas de card id
  cl.id_card AS id_card_comercial_legal,
  pc.id_card AS id_card_promesa_de_compraventa,
  es.id_card AS id_card_escrituracion,

  -- Lógica para determinar si el correo del broker Comercial Legal y el correo del broker Promesa de Compraventa coinciden; si coinciden hablamos del mismo negocio
  CASE 
    WHEN cl.correo_del_broker = pc.correo_del_broker 
    THEN 'CHECK'
    ELSE 'NO CHECK'
  END AS check_broker_ci_pc,

  -- Lógica para determinar si el correo del broker de Promesa de Compraventa y el correo del broker de Escrituración coinciden; si coinciden hablamos del mismo negocio
  CASE 
    WHEN pc.correo_del_broker = es.correo_del_broker 
    THEN 'CHECK'
    ELSE 'NO CHECK'
  END AS check_broker_pc_es,

  -- Lógica para determinar si el agente comercial de Comercial Legal y el agente comercial de Promesa de Compraventa coinciden; si coinciden hablamos del mismo negocio
  CASE 
    WHEN cl.comercial = pc.comercial
    THEN 'CHECK'
    ELSE 'NO CHECK'
  END AS check_comercial_ci_pc,

  -- Lógica para determinar si el agente comercial de Promesa de Compraventa y el agente comercial de Escrituración coinciden; si coinciden hablamos del mismo negocio
  CASE 
    WHEN cl.comercial = es.comercial
    THEN 'CHECK'
    ELSE 'NO CHECK'
  END AS check_comercial_pc_es,

  -- Lógica para determinar si la cedula Comercial Legal y la cedula Promesa de Compraventa coinciden; si coinciden hablamos del mismo negocio
  CASE 
    WHEN cl.cedula = pc.cedula
    THEN 'CHECK'
    ELSE 'NO CHECK'
  END AS check_cedula_ci_pc,

  -- Lógica para determinar si la cedula Promesa de Compraventa y la cedula Escrituración coinciden; si coinciden hablamos del mismo negocio
  CASE 
    WHEN pc.cedula = es.cedula 
    THEN 'CHECK'
    ELSE 'NO CHECK'
  END AS check_cedula_pc_es,

  -- Lógica para determinar el estatus del negocio, basado en el estatus de los Pipes
  CASE 
    WHEN cl.estatus = 'CIERRE' 
      AND pc.estatus = 'CIERRE'
      AND es.estatus = 'CIERRE'
    THEN 'CIERRE'
    WHEN cl.estatus = 'CIERRE' 
      AND pc.estatus = 'CIERRE'
      AND es.estatus = 'DESISTIDO'
    THEN 'DESISTIDO'
    WHEN cl.estatus = 'CIERRE' 
      AND pc.estatus = 'DESISTIDO'
      AND es.estatus = 'DESISTIDO'
    THEN 'DESISTIDO'
    WHEN cl.estatus = 'DESISTIDO' 
      AND pc.estatus = 'DESISTIDO'
      AND es.estatus IS NULL
    THEN 'DESISTIDO'
    WHEN cl.estatus = 'CIERRE' 
      AND pc.estatus = 'EN PROCESO'
      AND es.estatus IS NULL
    THEN 'EN PROCESO'
    WHEN cl.estatus = 'CIERRE' 
      AND pc.estatus = 'DESISTIDO'
      AND es.estatus IS NULL
    THEN 'DESISTIDO'
    WHEN cl.estatus = 'DESISTIDO' 
      AND pc.estatus IS NULL
      AND es.estatus IS NULL
    THEN 'DESISTIDO'
    WHEN cl.estatus = 'EN PROCESO' 
      AND pc.estatus IS NULL
      AND es.estatus IS NULL
    THEN 'EN PROCESO'
    WHEN cl.estatus IS NULL 
    THEN 'REVISAR'
    WHEN cl.estatus = 'CIERRE' 
      AND pc.estatus = 'DESISTIDO'
      AND es.estatus IS NULL
    THEN 'DESISTIDOO'
    WHEN cl.estatus = 'DESISTIDO' 
      AND pc.estatus = 'CIERRE'
      AND es.estatus = 'DESISTIDO'
    THEN 'REVISAR'
    WHEN cl.estatus = 'CIERRE' 
      AND pc.estatus = 'EN PROCESO'
      AND es.estatus = 'CIERRE'
    THEN 'REVISAR'
    WHEN cl.estatus = 'CIERRE' 
      AND pc.estatus = 'DESISTIDO'
      AND es.estatus = 'CIERRE'
    THEN 'REVISAR'
    WHEN cl.estatus = 'DESISTIDO' 
      AND pc.estatus = 'DESISTIDO'
      AND es.estatus = 'CIERRE'
    THEN 'REVISAR'
    WHEN cl.estatus = 'DESISTIDO' 
      AND pc.estatus = 'EN PROCESO'
      AND es.estatus = 'CIERRE'
    THEN 'REVISAR'
    WHEN cl.estatus = 'EN PROCESO' 
      AND pc.estatus = 'EN PROCESO'
      AND es.estatus = 'CIERRE'
    THEN 'REVISAR'
    WHEN cl.estatus = 'EN PROCESO' 
      AND pc.estatus = 'DESISTIDO'
      AND es.estatus = 'CIERRE'
    THEN 'REVISAR'
    ELSE 'EN PROCESO'
  END AS status,

  -- Lógica para determinar el nombre de la Fase en la que se encuentra el negocio
  CASE
    WHEN cl.fecha_crea_card > pc.fecha_crea_card 
    AND  cl.fecha_crea_card > es.fecha_crea_card
    THEN cl.name_phase
    WHEN pc.fecha_crea_card is null
    AND es.fecha_crea_card is null
    THEN cl.name_phase
    WHEN pc.fecha_crea_card > cl.fecha_crea_card 
    AND  pc.fecha_crea_card > es.fecha_crea_card
    THEN pc.name_phase
    WHEN es.fecha_crea_card is null
    AND cl.fecha_crea_card > pc.fecha_crea_card
    THEN cl.name_phase
    WHEN es.fecha_crea_card is null
    AND pc.fecha_crea_card > cl.fecha_crea_card
    THEN pc.name_phase
    WHEN es.fecha_crea_card > cl.fecha_crea_card 
    AND  es.fecha_crea_card > pc.fecha_crea_card
    THEN es.name_phase
  END AS name_phase,

  -- Lógica para determinar el Tipo de Venta en el que se encuentra el negocio
  CASE
    WHEN cl.fecha_crea_card > pc.fecha_crea_card 
    AND  cl.fecha_crea_card > es.fecha_crea_card
    THEN cl.tipo_de_venta
    WHEN pc.fecha_crea_card is null
    AND es.fecha_crea_card is null
    THEN cl.tipo_de_venta
    WHEN pc.fecha_crea_card > cl.fecha_crea_card 
    AND  pc.fecha_crea_card > es.fecha_crea_card
    THEN pc.tipo_de_venta
    WHEN es.fecha_crea_card is null
    AND cl.fecha_crea_card > pc.fecha_crea_card
    THEN cl.tipo_de_venta
    WHEN es.fecha_crea_card is null
    AND pc.fecha_crea_card > cl.fecha_crea_card
    THEN pc.tipo_de_venta
    WHEN es.fecha_crea_card > cl.fecha_crea_card 
    AND  es.fecha_crea_card > pc.fecha_crea_card
    THEN es.tipo_de_venta
  END AS tipo_de_venta,

  -- Lógica para determinar el nombre del Pipe en el que se encuentra el negocio
  CASE
    WHEN cl.fecha_crea_card > pc.fecha_crea_card 
    AND  cl.fecha_crea_card > es.fecha_crea_card
    THEN cl.name_pipe
    WHEN pc.fecha_crea_card is null
    AND es.fecha_crea_card is null
    THEN cl.name_pipe
    WHEN pc.fecha_crea_card > cl.fecha_crea_card 
    AND  pc.fecha_crea_card > es.fecha_crea_card
    THEN pc.name_pipe
    WHEN es.fecha_crea_card is null
    AND cl.fecha_crea_card > pc.fecha_crea_card
    THEN cl.name_pipe
    WHEN es.fecha_crea_card is null
    AND pc.fecha_crea_card > cl.fecha_crea_card
    THEN pc.name_pipe
    WHEN es.fecha_crea_card > cl.fecha_crea_card 
    AND  es.fecha_crea_card > pc.fecha_crea_card
    THEN es.name_pipe
  END AS name_pipe,

  -- Llamado de las columnas fecha_crea_card; indican la fecha en que fue creada la card Comercial Legal/Promesa de Compraventa/Escrituración
  cl.fecha_crea_card AS fecha_crea_card_comercial_legal, 
  pc.fecha_crea_card AS fecha_crea_card_promesa_de_compraventa,
  es.fecha_crea_card AS fecha_crea_card_escrituracion,

  -- Llamado de las columnas fecha_crea_ci, fecha_crea_pc, fecha_crea_escrituracion; indican la fecha en que se creo la Carta de intención/Promesa de Compraventa/Escrituración 
  cl.fecha_crea_ci,
  pc.fecha_crea_pc,
  es.fecha_crea_escrituracion,

  -- Llamado de las columnas first_time_card_in_phase renombrando para identificar de que Pipe provienen 
  cl.first_time_card_in_phase AS first_time_card_in_phase_ci,
  pc.first_time_card_in_phase AS first_time_card_in_phase_pc,
  es.first_time_card_in_phase AS first_time_card_in_phase_escrituracion,

  -- Llamado de las columnas fases_por_card renombrando para identificar de que Pipe provienen
  cl.fases_por_card AS fases_por_card_ci,
  pc.fases_por_card AS fases_por_card_pc,
  es.fases_por_card AS fases_por_card_es,

  -- Los tiempos correctos son calculados mas adelante con Python, sin embargo por el momento se dejan los calculos hechos en las siguientes líneas
  EXTRACT(DAY FROM pc.fecha_crea_card - cl.fecha_crea_card) AS tiempo_comercial_promesa_de_compraventa,
  EXTRACT(DAY FROM es.fecha_crea_card - pc.fecha_crea_card) AS tiempo_promesa_de_compraventa_escrituracion,  
  EXTRACT(DAY FROM pc.fecha_crea_pc - cl.first_time_card_in_phase) AS duracion_ci_pc,
  EXTRACT(DAY FROM es.fecha_crea_escrituracion - pc.first_time_card_in_phase) AS duracion_pc_es 

  -- Outers Join para obtener una tabla cruzando los datos de las 3 tablas enteriores, Modulo Comercial Legal, Promesa de Compraventa y Escrituración
  from buyers_modulo_comercial_legal AS cl
  full outer join buyers_promesa_de_compraventa AS pc
  on cl.nid = pc.nid
  full outer join buyers_escrituracion AS es
  on cl.nid = es.nid 

)

-- Consulta final realizada a la buyers_master ordenando las columnas de la manera solicitada

select
bm.nid,
bm.cedula_comercial_legal,
bm.cedula_promesa_de_compraventa,
bm.cedula_escrituracion,
bm.id_card_comercial_legal,
bm.id_card_promesa_de_compraventa,
bm.id_card_escrituracion,
bm.check_broker_ci_pc,
bm.check_broker_pc_es,
bm.check_comercial_ci_pc,
bm.check_comercial_pc_es,
bm.check_cedula_ci_pc,
bm.check_cedula_pc_es,
bm.status,
bm.name_phase,
bm.name_pipe,
bm.fases_por_card_ci,
bm.fases_por_card_pc,
bm.fases_por_card_es,
bm.tiempo_comercial_promesa_de_compraventa,
bm.tiempo_promesa_de_compraventa_escrituracion,
bm.duracion_ci_pc,
bm.duracion_pc_es,
bm.fecha_crea_ci,
bm.fecha_crea_pc,
bm.fecha_crea_escrituracion,
bm.first_time_card_in_phase_ci,
bm.first_time_card_in_phase_pc,
bm.first_time_card_in_phase_escrituracion,
bm.fecha_crea_card_comercial_legal,
bm.fecha_crea_card_promesa_de_compraventa,
bm.fecha_crea_card_escrituracion
from buyers_master AS bm
"""

query_job = client.query(query)  # Make an API request.

#Consulta en Query guardada en el DataFrame df_buyers_master
df_buyers_master = query_job.to_dataframe()

#Tiempos calculados correctamente en formato legible
df_buyers_master['tiempo_comercial_promesa_de_compraventa'] = df_buyers_master['fecha_crea_card_promesa_de_compraventa'] - df_buyers_master['fecha_crea_card_comercial_legal']
df_buyers_master['tiempo_promesa_de_compraventa_escrituracion'] = df_buyers_master['fecha_crea_card_escrituracion'] - df_buyers_master['fecha_crea_card_promesa_de_compraventa']
df_buyers_master['duracion_ci_pc'] = df_buyers_master['fecha_crea_pc'] - df_buyers_master['first_time_card_in_phase_ci']
df_buyers_master['duracion_pc_es'] = df_buyers_master['fecha_crea_escrituracion'] - df_buyers_master['first_time_card_in_phase_pc']

#Creación del DataFrame df_buyers_master_resumen quitando las columnas que no nos interesan en la vista
df_buyers_master_resumen = df_buyers_master.drop(['fecha_crea_card_promesa_de_compraventa','fecha_crea_card_comercial_legal','fecha_crea_card_escrituracion'], axis=1)

#Pruebas realizadas por Gian para verificar que la lógica esta en orden
#df_buyers_master = df_buyers_master[(df_buyers_master['duracion_ci_pc'] >= '0 days 00:00:00') | (df_buyers_master['duracion_ci_pc'].isnull())]
#df_buyers_master = df_buyers_master[(df_buyers_master['duracion_pc_es'] >= '0 days 00:00:00') | (df_buyers_master['duracion_pc_es'].isnull())]
#profile = ProfileReport(df_buyers_master,minimal=True)
#profile.to_file('/home/jurado/habi-git/Perfil_df_buyers_master_BQ.html')
#df_buyers_master_fullcheck = df_buyers_master[(df_buyers_master['check_cedula_ci_pc'] == 'CHECK') &
#                                          (df_buyers_master['check_broker_ci_pc'] == 'CHECK') &
#                                          (df_buyers_master['check_comercial_ci_pc'] == 'CHECK')]
#profile = ProfileReport(df_buyers_master_fullcheck,minimal=True)
#profile.to_file('/home/jurado/habi-git/Perfil_df_buyers_master_fullcheck_BQ.html')
#df_buyers_master_fullcheck_ci = df_buyers_master_fullcheck[(df_buyers_master_fullcheck['name_pipe'] == '1. Buyers Modulo Comercial/Legal [Prueba CO]')]
# profile = ProfileReport(df_buyers_master_fullcheck_ci,minimal=True)
# profile.to_file('/home/jurado/habi-git/Perfil_df_buyers_master_fullcheck_ci_BQ.html')
#df_buyers_master_fullcheck_pc = df_buyers_master_fullcheck[(df_buyers_master_fullcheck['name_pipe'] == 'Carta de Intención Buyers')]
#profile = ProfileReport(df_buyers_master_fullcheck_pc,minimal=True)
#profile.to_file('/home/jurado/habi-git/Perfil_df_buyers_master_fullchec_pc_BQ.html')
#df_buyers_master_fullcheck_es = df_buyers_master_fullcheck[(df_buyers_master_fullcheck['name_pipe'] == '2. Escrituración Buyers [Prueba CO] Amateratsu')]
#profile = ProfileReport(df_buyers_master_fullcheck_es,minimal=True)
#profile.to_file('/home/jurado/habi-git/Perfil_df_buyers_master_fullchec_es_BQ.html')
