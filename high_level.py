import json
from google.cloud import secretmanager
from typing import List

from googleapiclient.discovery import build
from google.cloud import bigquery
from google.oauth2 import service_account
import pandas as pd
import datetime
import numpy as np

def load_credentials(project_number: str, name_secret: str) -> dict:
    '''
    Funcion de carga de credenciales almacenadas en GCP Secret Manager
    
    Parameters
    ----------
        project_number : str
            Numero que identifica el proyecto en GCP.
        name_secret : str
            Nombre del secreto almacenado en el proyecto.
    
    Returns
    -------
    cred : json
        Credenciales en formato json.
    '''
    #TODO: La variable secret_id tiene quemado parte de su cadena, es necesario dejar de forma parametrica.

    # ID of the secret to create.
    secret_id = f"projects/{project_number}/secrets/{name_secret}/versions/latest"
    
    # Create the Secret Manager client.
    client = secretmanager.SecretManagerServiceClient()
    
    # Access the secret version.
    response = client.access_secret_version(request={"name": secret_id})
    
    # Extaccion del secreto 
    payload = response.payload.data.decode("UTF-8")
    
    #Save in dictionary
    cred = json.loads(payload)
    
    return cred

def extract_from_gsheet(scope: List[str], sheet_id: str, data_to_pull, cred: dict) -> pd.DataFrame:
    '''
    Lectura de hojas de calculo de Google Sheet.

    Para hacer la lectura se debe contar con:
        1. Credenciales de autorizacion para una aplicacion. Ver https://developers.google.com/sheets/api/quickstart/python#authorize_credentials_for_a_desktop_application
        2. La aplicacion debe terner permisos de lectura al archivo, es decir se debe agragar el mail ejemplo@nombre_proyecto_gcp.iam.gserviceaccount.com al documento.

    Parameters
    ----------
        scope: List[str]
            Lista con la URL de llamado al API. Ejemplo ['https://www.googleapis.com/auth/spreadsheets.readonly']
        sheet_id: str
            Identificador del documento en Google Sheet. Se toma de la URL del documento de la siguiente forma https://docs.google.com/spreadsheets/d/<sheet_id>/edit#gid=0
        data_to_pull: str
            Hoja y rango a extraer de los datos. Ejemplo 'Class Data!A2:E'
        cred: dict
            Credenciales extraidas de la funcion src.secrete.call.load_credentials
    
    Returns
    -------
    df: pd.DataFrame
        DataFrame con los datos de la hoja de calculo. El pd.DataFrame de retornado toma como primera fila el encabezado del DataFrame.
    '''
    #Construccion del servicio
    service = build(
        'sheets',
        'v4',
        credentials = service_account.Credentials.from_service_account_info(cred, scopes=scope))
    
    #Lectura del documento
    sheet = service.spreadsheets()

    #Extraccion de los valores en la hoja de calculo
    result = sheet.values().get(
        spreadsheetId=sheet_id,
        range=data_to_pull,
        valueRenderOption='UNFORMATTED_VALUE').execute()
    values = result.get('values', [])

    #Validacion de datos no nulos
    if not values:
        print('No data found.')
    else:
        #convert values into dataframe
        df = pd.DataFrame(values[1:], columns= values[0])
        return df


def read_gsheet() -> pd.DataFrame:
    '''
    Lectura de datos de comite almacenados en GSheet

    Returns
    -------
    df: pd.DataFrame
        Conjutno de datos almacenado en el archivo de GSheet    
    '''

    #Extraccion de secreto
    gsheet_cred = load_credentials(**credentials['google_sheet'])
    #Carga de google sheet en dataframe
    df = extract_from_gsheet(
        scope = ['https://www.googleapis.com/auth/spreadsheets.readonly'],
        cred = gsheet_cred,
        **google_sheet['data'])   
    get_dagster_logger().info(f"Se encontraron {df.shape[0]} registros y {df.shape[1]} columnas")
    return df

def trans_entero(cadena):
    if cadena=='':
        cadena=cadena
    elif cadena is None:
        cadena=cadena
    else:
        try:
            cadena=int(cadena)
        except:
            cadena=cadena
    return cadena

def exceldate(serial):
    serial=trans_entero(serial)
    if serial is not None:
        try:
            seconds = (serial - 25569) * 86400
            d = datetime.datetime.utcfromtimestamp(seconds)
            d.strftime('%d-%m-%y')
        except:
            d=serial
    else:
        try:
            seconds = (0 - 25569) * 86400.0
            d = datetime.datetime.utcfromtimestamp(seconds)
            d.strftime('%d-%m-%y')
        except:
            d=serial
    return d

cred = load_credentials('372328156728', 'data_governance_key')
scope = ['https://www.googleapis.com/auth/spreadsheets.readonly']
sheet_id = '1FR03EAnghkrlEq7NJsclGcq6DPncotBn4T6uB9GG1iE'
data_to_pull = 'High Level Ventas'

df_high_level_ventas = extract_from_gsheet(scope, sheet_id, data_to_pull, cred)
df_high_level_ventas=df_high_level_ventas.rename(columns={'Fecha Escritura Real':'fecha_escritura_real','Fecha Carta de Intencion':'fecha_carta_de_intencion','Fecha Firma Promesa':'fecha_firma_promesa'})
df_high_level_ventas['fecha_escritura_real'] = [exceldate(i)for i in df_high_level_ventas['fecha_escritura_real']]
df_high_level_ventas['fecha_carta_de_intencion'] = [exceldate(i)for i in df_high_level_ventas['fecha_carta_de_intencion']]
df_high_level_ventas['fecha_firma_promesa'] = [exceldate(i)for i in df_high_level_ventas['fecha_firma_promesa']]

df_high_level_ventas_resumen = df_high_level_ventas[['NID','fecha_carta_de_intencion','fecha_firma_promesa','fecha_escritura_real']]

conditions = [(df_high_level_ventas['fecha_carta_de_intencion']=='') | (df_high_level_ventas['fecha_carta_de_intencion'].isnull()),  (df_high_level_ventas['fecha_carta_de_intencion'].notnull())]
values=['NO CHECK','CHECK']
df_high_level_ventas_resumen['check_ci'] = np.select(conditions,values)

conditions = [(df_high_level_ventas['fecha_firma_promesa']=='') | (df_high_level_ventas['fecha_firma_promesa'].isnull()),  (df_high_level_ventas['fecha_firma_promesa'].notnull())]
values=['NO CHECK','CHECK']
df_high_level_ventas_resumen['check_pc'] = np.select(conditions,values)

conditions = [(df_high_level_ventas['fecha_escritura_real']=='') | (df_high_level_ventas['fecha_escritura_real'].isnull()),  (df_high_level_ventas['fecha_escritura_real'].notnull())]
values=['NO CHECK','CHECK']
df_high_level_ventas_resumen['check_es'] = np.select(conditions,values)


df_high_level_ventas_resumen['fecha_carta_de_intencion'] = pd.to_datetime(df_high_level_ventas_resumen['fecha_carta_de_intencion'], format="%Y-%m-%d")
df_high_level_ventas_resumen['fecha_escritura_real'] = pd.to_datetime(df_high_level_ventas_resumen['fecha_escritura_real'], format="%Y-%m-%d")
df_high_level_ventas_resumen['fecha_firma_promesa'] = pd.to_datetime(df_high_level_ventas_resumen['fecha_firma_promesa'], format="%Y-%m-%d")

conditions = [(df_high_level_ventas_resumen['fecha_escritura_real'] > df_high_level_ventas_resumen['fecha_carta_de_intencion']) & (df_high_level_ventas_resumen['fecha_escritura_real'] > df_high_level_ventas_resumen['fecha_firma_promesa']),
              (df_high_level_ventas_resumen['fecha_carta_de_intencion'].isnull() ) & (df_high_level_ventas_resumen['fecha_escritura_real'] > df_high_level_ventas_resumen['fecha_firma_promesa']),
              (df_high_level_ventas_resumen['fecha_carta_de_intencion'].isnull() ) & (df_high_level_ventas_resumen['fecha_escritura_real'].notnull()) & (df_high_level_ventas_resumen['fecha_firma_promesa'].isnull()),  
              (df_high_level_ventas_resumen['fecha_carta_de_intencion'] > df_high_level_ventas_resumen['fecha_escritura_real']) & (df_high_level_ventas_resumen['fecha_carta_de_intencion'] > df_high_level_ventas_resumen['fecha_firma_promesa']),
              (df_high_level_ventas_resumen['fecha_carta_de_intencion'].isnull()) & (df_high_level_ventas_resumen['fecha_firma_promesa'].notnull()) & (df_high_level_ventas_resumen['fecha_escritura_real'].isnull()),
              (df_high_level_ventas_resumen['fecha_carta_de_intencion'] < df_high_level_ventas_resumen['fecha_firma_promesa']) & (df_high_level_ventas_resumen['fecha_escritura_real'].isnull()),  
              (df_high_level_ventas_resumen['fecha_firma_promesa'] > df_high_level_ventas_resumen['fecha_escritura_real']) & (df_high_level_ventas_resumen['fecha_firma_promesa'] > df_high_level_ventas_resumen['fecha_carta_de_intencion']),
              (df_high_level_ventas_resumen['fecha_carta_de_intencion'].notnull()) & (df_high_level_ventas_resumen['fecha_firma_promesa'].isnull()) & (df_high_level_ventas_resumen['fecha_escritura_real'].isnull()),
              (df_high_level_ventas_resumen['fecha_carta_de_intencion'].isnull()) & (df_high_level_ventas_resumen['fecha_firma_promesa'].isnull()) & (df_high_level_ventas_resumen['fecha_escritura_real'].isnull())]

values=['2. Escrituración Buyers [Prueba CO] Amateratsu','2. Escrituración Buyers [Prueba CO] Amateratsu','2. Escrituración Buyers [Prueba CO] Amateratsu',
        '1. Buyers Modulo Comercial/Legal [Prueba CO]','1. Buyers Modulo Comercial/Legal [Prueba CO]','1. Buyers Modulo Comercial/Legal [Prueba CO]',
        'Carta de Intención Buyers','Carta de Intención Buyers','Revisar']

df_high_level_ventas_resumen['name_pipe'] = np.select(conditions,values)

df_high_level_ventas_resumen = df_high_level_ventas_resumen[(df_high_level_ventas_resumen['fecha_escritura_real'] < '2022-11-01') & 
                                    (df_high_level_ventas_resumen['fecha_escritura_real'] > '1899-12-30') | 
                                    (df_high_level_ventas_resumen['fecha_escritura_real'].isnull())]

df_high_level_ventas_resumen = df_high_level_ventas_resumen[(df_high_level_ventas_resumen['fecha_firma_promesa'] < '2022-11-01') & 
                                    (df_high_level_ventas_resumen['fecha_firma_promesa'] > '1899-12-30') | 
                                    (df_high_level_ventas_resumen['fecha_firma_promesa'].isnull())]

df_high_level_ventas_resumen = df_high_level_ventas_resumen[(df_high_level_ventas_resumen['fecha_carta_de_intencion'] < '2022-11-01') & 
                                    (df_high_level_ventas_resumen['fecha_carta_de_intencion'] > '1899-12-30') | 
                                    (df_high_level_ventas_resumen['fecha_carta_de_intencion'].isnull())]

df_high_level_ventas_resumen